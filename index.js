console.log("hello cami");
console.log(document);
//result: (document HTML code) targets the whole web page, index.html
const txtFirstName = document.querySelector("#txt-first-name");
console.log(txtFirstName);
//result: input field/tag <input...
/*
	alternative ways:
		>>document.getElementById("txt-first-name");
		>>document.getElementByClassName("text-class");
		>>document.getElementByTagName("h1")
			(identify their selectors)
			(query selector is more flexible)
*/
const txtLastName = document.querySelector("#txt-last-name");
console.log(txtLastName);
	//TARGET THE FULL NAME
let spanFullName = document.querySelector("#span-full-name");
console.log(spanFullName);
	//EVENT LISTENERS
	//event can have shorthand (e)
	//pertaining to event
// txtFirstName.addEventListener('keyup', (event) =>{
// 	spanFullName.innerHTML = txtFirstName.value;
// });
// txtLastName.addEventListener('keyup', (event) =>{
// 	spanFullName.innerHTML = txtLastName.value;
// })
//event keyup pag alis ng kamay sa keyboard
//pangalawang argument is yung function
//spanFullName holds the id span-full-name
//nagkakalaman gawa ng innerHTML
//innerHTML, assigned value input, first name

// txtFirstName.addEventListener('keyup',(event)=>{
// 	console.log(event);
// 	console.log(event.target);
// 	console.log(event.target.value);
// })

//console.log(event); displays everytime u keyup

////////////////////stretch
// const keyCodeEvent = (e) => {
// 	let kc = e.keyCode;
// 	if (kc === 65){
// 		e.target.value = null;
// 		alert("Someone cliked a!")
// 	}
// };
// txtFirstName.addEventListener('keyup', keyCodeEvent);


/////////s45-activity//////////
//no need for anonymous function
//fullNameEvent will concatenate txtFirstName and txtLastName in the spanFullName
const fullNameEvent = (e) => {
	spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
};

//(not working)
//spanFullName.addEventListener('keyup',fullNameEvent);

//(working)
//need to invoke for each
txtFirstName.addEventListener('keyup', fullNameEvent);
txtLastName.addEventListener('keyup',fullNameEvent);

